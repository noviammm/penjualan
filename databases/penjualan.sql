/*
SQLyog Professional v12.4.3 (64 bit)
MySQL - 10.1.31-MariaDB : Database - penjualan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`penjualan` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `penjualan`;

/*Table structure for table `tbl_barang` */

DROP TABLE IF EXISTS `tbl_barang`;

CREATE TABLE `tbl_barang` (
  `kode_barang` varchar(50) NOT NULL,
  `nama_barang` varchar(40) NOT NULL,
  `harga_jual` double NOT NULL,
  `harga_beli` double NOT NULL,
  `kode_satuan` int(11) NOT NULL,
  `kode_kategori` int(11) NOT NULL,
  `stok_barang` int(11) DEFAULT NULL,
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_barang` */

insert  into `tbl_barang`(`kode_barang`,`nama_barang`,`harga_jual`,`harga_beli`,`kode_satuan`,`kode_kategori`,`stok_barang`) values 
('BRG1','A',5000,2500,1,2,50),
('BRG2','B',10000,5000,1,3,100),
('BRG3','C',15000,7500,1,1,150);

/*Table structure for table `tbl_kategori` */

DROP TABLE IF EXISTS `tbl_kategori`;

CREATE TABLE `tbl_kategori` (
  `kode_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(35) NOT NULL,
  PRIMARY KEY (`kode_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_kategori` */

insert  into `tbl_kategori`(`kode_kategori`,`kategori`) values 
(1,'Ban'),
(2,'Busi'),
(3,'Gear');

/*Table structure for table `tbl_konsumen` */

DROP TABLE IF EXISTS `tbl_konsumen`;

CREATE TABLE `tbl_konsumen` (
  `kode_konsumen` varchar(50) NOT NULL,
  `nama_konsumen` varchar(70) NOT NULL,
  `alamat` varchar(185) NOT NULL,
  `telp` varchar(15) DEFAULT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(55) NOT NULL,
  PRIMARY KEY (`kode_konsumen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_konsumen` */

insert  into `tbl_konsumen`(`kode_konsumen`,`nama_konsumen`,`alamat`,`telp`,`password`,`email`) values 
('K001','Novia','Tanjung Barat','0999999999','novia','noviamw@gmail.com'),
('K002','Nana','Pinang Ranti','0888888888','nana','nana@gmail.com'),
('K003','Nisa','Condet','07777777','nisa','nisa@gmail.com');

/*Table structure for table `tbl_penjualan` */

DROP TABLE IF EXISTS `tbl_penjualan`;

CREATE TABLE `tbl_penjualan` (
  `no_faktur` varchar(50) NOT NULL,
  `tgl_faktur` date NOT NULL,
  `kode_konsumen` varchar(50) DEFAULT NULL,
  `kode_barang` varchar(50) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga_satuan` double DEFAULT NULL,
  `harga_total` double NOT NULL,
  PRIMARY KEY (`no_faktur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_penjualan` */

insert  into `tbl_penjualan`(`no_faktur`,`tgl_faktur`,`kode_konsumen`,`kode_barang`,`jumlah`,`harga_satuan`,`harga_total`) values 
('PJ001','2018-09-05','K001','BRG1',3,10000,300000),
('PJ00100','2018-09-05','K002','BRG1',100,100,1000),
('PJ002','2018-09-05','K001','BRG1',5,10000,50000);

/*Table structure for table `tbl_satuan` */

DROP TABLE IF EXISTS `tbl_satuan`;

CREATE TABLE `tbl_satuan` (
  `kode_satuan` int(11) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(30) NOT NULL,
  PRIMARY KEY (`kode_satuan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_satuan` */

insert  into `tbl_satuan`(`kode_satuan`,`satuan`) values 
(1,'item'),
(2,'pack'),
(3,'unit');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

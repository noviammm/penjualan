package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.List;
import java.util.ArrayList;
import model.BarangModel;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Aplikasi Penjualan Spareparts</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("    <center><h1>Tampil Master Data Barang</h1></center>\n");
      out.write("    <table style=\"margin:20px auto;\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\n");
      out.write("        <tr>\n");
      out.write("            <th>Kode Barang</th>\n");
      out.write("            <th>Nama Barang</th>\n");
      out.write("            <th>Harga Jual</th>\n");
      out.write("            <th>Harga Beli</th>\n");
      out.write("            <th>Kode Satuan</th>\n");
      out.write("            <th>Kode Kategori</th>\n");
      out.write("            <th>Action</th>\n");
      out.write("        </tr>\n");
      out.write("        ");

            BarangModel bm = new BarangModel();
            List<BarangModel> data = new ArrayList<BarangModel>();
            String ket = request.getParameter("ket");
            if (ket == null) {
                data = bm.tampil();
            }
            for (int x = 0; x < data.size(); x++) {
        
      out.write("\n");
      out.write("        <tr>\n");
      out.write("<!--            <td>");
      out.print(x + 1);
      out.write("</td>-->\n");
      out.write("            <td>");
      out.print(data.get(x).getKode_barang());
      out.write("</td>\n");
      out.write("            <td>");
      out.print(data.get(x).getNama_barang());
      out.write("</td>\n");
      out.write("            <td>");
      out.print(data.get(x).getHarga_jual());
      out.write("</td>\n");
      out.write("            <td>");
      out.print(data.get(x).getHarga_beli());
      out.write("</td>\n");
      out.write("            <td>");
      out.print(data.get(x).getKode_satuan());
      out.write("</td>\n");
      out.write("            <td>");
      out.print(data.get(x).getKode_kategori());
      out.write("</td>\n");
      out.write("            <td>\n");
      out.write("                <a href=\"BarangController?proses=edit-barang&id=");
      out.print(data.get(x).getKode_barang());
      out.write("\">Edit</a>\n");
      out.write("                <a href=\"BarangController?proses=hapus-barang&id=");
      out.print(data.get(x).getKode_barang());
      out.write("\">Hapus</a>\n");
      out.write("            </td>\n");
      out.write("        </tr>\n");
      out.write("        \n");
      out.write("        ");
}
      out.write("\n");
      out.write("    </table>\n");
      out.write("    <center>\n");
      out.write("    <a class=\"tambah\" href=\"BarangController?proses=input-barang\">Tambah</a>\n");
      out.write("</center>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}

<%-- 
    Document   : tambahKonsumen
    Created on : Sep 6, 2018, 11:00:53 AM
    Author     : Novia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aplikasi Penjualan Spareparts</title>
    </head>
    <body>
        <center><h1>Tambah Konsumen</h1></center>
        <form action="KonsumenController?data=konsumen&proses=input-konsumen" method="post">
            <table style="margin:20px auto;">
                <tr>
                    <td>Kode Konsumen</td>
                    <td><input type="text" name="kode_konsumen"></td>
                </tr>
                <tr>
                    <td>Nama Konsumen</td>
                    <td><input type="text" name="nama_konsumen"></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td><input type="text" name="alamat"></td>
                </tr>
                <tr>
                    <td>Telepon</td>
                    <td><input type="text" name="telp"></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="text" name="password"></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><input type="text" name="email"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="TAMBAH"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="KEMBALI"></td>
                </tr>
            </table>
        </form>
</body>
</html>

<%-- 
    Document   : editBarang
    Created on : Sep 5, 2018, 4:47:58 PM
    Author     : Novia
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.BarangModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aplikasi Penjualan Spareparts</title>
    </head>
    <body>
        <center><h1>Edit Barang</h1></center>
        <form action="BarangController?data=barang&proses=update-barang" method="post">
            <table style="margin:20px auto;">
            <%
                String id = request.getParameter("id");
                BarangModel bm = new BarangModel();
                bm.setKode_barang(id);
                List<BarangModel> data = new ArrayList<BarangModel>();
                data = bm.cariID(); 
                if (data.size() > 0) {
            %>
                <tr>
                    <td>Kode Barang</td>
                    <td><input type="text" name="kode_barang" value="<%=data.get(0).getKode_barang()%>"></td>
                </tr>
                <tr>
                    <td>Nama Barang</td>
                    <td><input type="text" name="nama_barang" value="<%=data.get(0).getNama_barang()%>"></td>
                    <td><input type="hidden" name="id" value="<%=data.get(0).getKode_barang()%>"></td>
                </tr>
                <tr>
                    <td>Harga Jual</td>
                    <td><input type="text" name="harga_jual" value="<%=data.get(0).getHarga_jual()%>"></td>
                    <td><input type="hidden" name="id" value="<%=data.get(0).getKode_barang()%>"></td>
                </tr>
                <tr>
                    <td>Harga Beli</td>
                    <td><input type="text" name="harga_beli" value="<%=data.get(0).getHarga_beli()%>"></td>
                    <td><input type="hidden" name="id" value="<%=data.get(0).getKode_barang()%>"></td>
                </tr>
                <tr>
                    <td>Kode Satuan</td>
                    <td><input type="text" name="kode_satuan" value="<%=data.get(0).getKode_satuan()%>"></td>
                    <td><input type="hidden" name="id" value="<%=data.get(0).getKode_barang()%>"></td>
                </tr>
                <tr>
                    <td>Kode Kategori</td>
                    <td><input type="text" name="kode_kategori" value="<%=data.get(0).getKode_kategori()%>"></td>
                    <td><input type="hidden" name="id" value="<%=data.get(0).getKode_barang()%>"></td>
                </tr>
                <tr>
                    <td>Stok Barang</td>
                    <td><input type="text" name="stok_barang" value="<%=data.get(0).getStok_barang()%>"></td>
                    <td><input type="hidden" name="id" value="<%=data.get(0).getKode_barang()%>"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="EDIT"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="KEMBALI"></td>
                </tr>
                  <%}%>
            </table>
        </form> 
    </body>
</html>

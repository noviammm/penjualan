<%-- 
    Document   : BarangView
    Created on : Sep 5, 2018, 8:07:13 PM
    Author     : Novia
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.BarangModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aplikasi Penjualan Spareparts</title>
    </head>
    <body>
    <center><h1>Master Data Barang</h1></center>
    <table style="margin:20px auto;" border="1" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Harga Jual</th>
            <th>Harga Beli</th>
            <th>Kode Satuan</th>
            <th>Kode Kategori</th>
            <th>Stok Barang</th>
            <th>Action</th>
        </tr>
        <%
            BarangModel bm = new BarangModel();
            List<BarangModel> data = new ArrayList<BarangModel>();
            String ket = request.getParameter("ket");
            if (ket == null) {
                data = bm.tampil();
            }
            for (int x = 0; x < data.size(); x++) {
        %>
        <tr>
<!--            <td><%=x + 1%></td>-->
            <td><%=data.get(x).getKode_barang()%></td>
            <td><%=data.get(x).getNama_barang()%></td>
            <td><%=data.get(x).getHarga_jual()%></td>
            <td><%=data.get(x).getHarga_beli()%></td>
            <td><%=data.get(x).getKode_satuan()%></td>
            <td><%=data.get(x).getKode_kategori()%></td>
            <td><%=data.get(x).getStok_barang()%></td>
            <td>
                <a href="BarangController?proses=edit-barang&id=<%=data.get(x).getKode_barang()%>">Edit</a>
                <a href="BarangController?proses=hapus-barang&id=<%=data.get(x).getKode_barang()%>">Hapus</a>
            </td>
        </tr>
        
        <%}%>
    </table>
    <center>
        <td> | </td>
        <a class="tambah" href="BarangController?proses=input-barang">TAMBAH</a>
        <td> | </td>
        <br>
        <br>
        <br>
        <td> | </td>
        <a class="" href="index.jsp">KEMBALI</a>
        <td> | </td>
    </center>
</body>
</html>

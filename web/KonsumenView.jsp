<%-- 
    Document   : KonsumenView
    Created on : Sep 6, 2018, 10:35:16 AM
    Author     : Novia
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.KonsumenModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aplikasi Penjualan Spareparts</title>
    </head>
    <body>
    <center><h1>Data Konsumen</h1></center>
    <table style="margin:20px auto;" border="1" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <th>Kode Konsumen</th>
            <th>Nama Konsumen</th>
            <th>Alamat</th>
            <th>Telepon</th>
<!--            <th>Password</th>-->
            <th>Email</th>
            <th>Action</th>
        </tr>
        <%
            KonsumenModel km = new KonsumenModel();
            List<KonsumenModel> data = new ArrayList<KonsumenModel>();
            String ket = request.getParameter("ket");
            if (ket == null) {
                data = km.tampil();
            }
            for (int x = 0; x < data.size(); x++) {
        %>
        <tr>
<!--            <td><%=x + 1%></td>-->
            <td><%=data.get(x).getKode_konsumen()%></td>
            <td><%=data.get(x).getNama_konsumen()%></td>
            <td><%=data.get(x).getAlamat()%></td>
            <td><%=data.get(x).getTelp()%></td>
<!--            <td><%=data.get(x).getPassword()%></td>-->
            <td><%=data.get(x).getEmail()%></td>
            <td>
                <a href="KonsumenController?proses=edit-konsumen&id=<%=data.get(x).getKode_konsumen()%>">Edit</a>
                <a href="KonsumenController?proses=hapus-konsumen&id=<%=data.get(x).getKode_konsumen()%>">Hapus</a>
            </td>
        </tr>
        
        <%}%>
    </table>
    <center>
        <td> | </td>
        <a class="tambah" href="KonsumenController?proses=input-konsumen">TAMBAH</a>
        <td> | </td>
        <br>
        <br>
        <br>
        <td> | </td>
        <a class="" href="index.jsp">KEMBALI</a>
        <td> | </td>
</center>
</body>
</html>

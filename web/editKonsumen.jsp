<%-- 
    Document   : editKonsumen
    Created on : Sep 6, 2018, 11:01:07 AM
    Author     : Novia
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.KonsumenModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aplikasi Penjualan Spareparts</title>
    </head>
    <body>
        <center><h1>Edit Konsumen</h1></center>
        <form action="KonsumenController?data=konsumen&proses=update-konsumen" method="post">
            <table style="margin:20px auto;">
            <%
                String id = request.getParameter("id");
                KonsumenModel km = new KonsumenModel();
                km.setKode_konsumen(id);
                List<KonsumenModel> data = new ArrayList<KonsumenModel>();
                data = km.cariID(); 
                if (data.size() > 0) {
            %>
                <tr>
                    <td>Kode Konsumen</td>
                    <td><input type="text" name="kode_konsumen" value="<%=data.get(0).getKode_konsumen()%>"></td>
                </tr>
                <tr>
                    <td>Nama Konsumen</td>
                    <td><input type="text" name="nama_konsumen" value="<%=data.get(0).getNama_konsumen()%>"></td>
                    <td><input type="hidden" name="id" value="<%=data.get(0).getKode_konsumen()%>"></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td><input type="text" name="alamat" value="<%=data.get(0).getAlamat()%>"></td>
                    <td><input type="hidden" name="id" value="<%=data.get(0).getKode_konsumen()%>"></td>
                </tr>
                <tr>
                    <td>Telepon</td>
                    <td><input type="text" name="telp" value="<%=data.get(0).getTelp()%>"></td>
                    <td><input type="hidden" name="id" value="<%=data.get(0).getKode_konsumen()%>"></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="text" name="password" value="<%=data.get(0).getPassword()%>"></td>
                    <td><input type="hidden" name="id" value="<%=data.get(0).getKode_konsumen()%>"></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><input type="text" name="email" value="<%=data.get(0).getEmail()%>"></td>
                    <td><input type="hidden" name="id" value="<%=data.get(0).getKode_konsumen()%>"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="EDIT"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="KEMBALI"></td>
                </tr>
                  <%}%>
            </table>
        </form> 
    </body>
</html>

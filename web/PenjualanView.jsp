<%-- 
    Document   : PenjualanView
    Created on : Sep 5, 2018, 8:41:28 PM
    Author     : Novia
--%>

<%@page import="model.PenjualanModel"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.BarangModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aplikasi Penjualan Spareparts</title>
    </head>
    <body>
    <center><h1>Penjualan Spareparts</h1></center>
    <table style="margin:20px auto;" border="1" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <th>No Faktur</th>
            <th>Tanggal Faktur</th>
            <th>Kode Konsumen</th>
            <th>Kode Barang</th>
            <th>Jumlah</th>
            <th>Harga Satuan</th>
            <th>Harga Total</th>
            <th>Action</th>
        </tr>
        <%
            PenjualanModel pm = new PenjualanModel();
            List<PenjualanModel> data = new ArrayList<PenjualanModel>();
            String ket = request.getParameter("ket");
            if (ket == null) {
                data = pm.tampil();
            }
            for (int x = 0; x < data.size(); x++) {
        %>
        <tr>
<!--            <td><%=x + 1%></td>-->
            <td><%=data.get(x).getNo_faktur()%></td>
            <td><%=data.get(x).getTgl_faktur()%></td>
            <td><%=data.get(x).getKode_konsumen()%></td>
            <td><%=data.get(x).getKode_barang()%></td>
            <td><%=data.get(x).getJumlah()%></td>
            <td><%=data.get(x).getHarga_satuan()%></td>
            <td><%=data.get(x).getHarga_total()%></td>
            <td>
                <a href="PenjualanController?proses=hapus-penjualan&id=<%=data.get(x).getNo_faktur()%>">Hapus</a>
            </td>
        </tr>
        
        <%}%>
    </table>
    <center>
        <td> | </td>
        <a class="" href="PenjualanController?proses=input-penjualan">INPUT</a>
        <td> | </td>
        <br>
        <br>
        <br>
        <td> | </td>
        <a class="" href="index.jsp">KEMBALI</a>
        <td> | </td>
    </center>
</body>
</html>

<%-- 
    Document   : BarangView
    Created on : Sep 5, 2018, 1:48:03 PM
    Author     : Novia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aplikasi Penjualan Spareparts</title>
    </head>
    <body>
        <center><h1>Input Barang</h1></center>
        <form action="BarangController?data=barang&proses=input-barang" method="post">
            <table style="margin:20px auto;">
                <tr>
                    <td>Kode Barang</td>
                    <td><input type="text" name="kode_barang"></td>
                </tr>
                <tr>
                    <td>Nama Barang</td>
                    <td><input type="text" name="nama_barang"></td>
                </tr>
                <tr>
                    <td>Harga Jual</td>
                    <td><input type="text" name="harga_jual"></td>
                </tr>
                <tr>
                    <td>Harga Beli</td>
                    <td><input type="text" name="harga_beli"></td>
                </tr>
                <tr>
                    <td>Kode Satuan</td>
                    <td><input type="text" name="kode_satuan"></td>
                </tr>
                <tr>
                    <td>Kode Kategori</td>
                    <td><input type="text" name="kode_kategori"></td>
                </tr>
                <tr>
                    <td>Stok Barang</td>
                    <td><input type="text" name="stok_barang"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="TAMBAH"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="KEMBALI"></td>
                </tr>
            </table>
        </form>
</body>
</html>

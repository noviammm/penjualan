<%-- 
    Document   : inputPenjualan
    Created on : Sep 5, 2018, 9:33:31 PM
    Author     : Novia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Aplikasi Penjualan Spareparts</title>
    </head>
    <body>
        <center><h1>Input Penjualan</h1></center>
        <form action="PenjualanController?data=penjualan&proses=input-penjualan" method="post">
            <table style="margin:20px auto;">
                <tr>
                    <td>No Faktur</td>
                    <td><input type="text" name="no_faktur"></td>
                </tr>
                <tr>
                    <td>Tgl Faktur</td>
                    <td><input type="text" name="tgl_faktur"></td>
                </tr>
                <tr>
                    <td>Kode Konsumen</td>
                    <td><input type="text" name="kode_konsumen"></td>
                </tr>
                <tr>
                    <td>Kode Barang</td>
                    <td><input type="text" name="kode_barang"></td>
                </tr>
                <tr>
                    <td>Jumlah</td>
                    <td><input type="text" name="jumlah"></td>
                </tr>
                <tr>
                    <td>Harga Satuan</td>
                    <td><input type="text" name="harga_satuan"></td>
                </tr>
                <tr>
                    <td>Harga Total</td>
                    <td><input type="text" name="harga_total"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="INPUT"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="KEMBALI"></td>
                </tr>
            </table>
        </form>
</body>
</html>

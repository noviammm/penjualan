
package model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import koneksi.koneksi;

public class PenjualanModel {
    String no_faktur, tgl_faktur, kode_konsumen, kode_barang,
            jumlah, harga_satuan, harga_total;
    
    koneksi db = null;
    
    public PenjualanModel(){
        db = new koneksi();
    }

    public String getNo_faktur() {
        return no_faktur;
    }

    public void setNo_faktur(String no_faktur) {
        this.no_faktur = no_faktur;
    }

    public String getTgl_faktur() {
        return tgl_faktur;
    }

    public void setTgl_faktur(String tgl_faktur) {
        this.tgl_faktur = tgl_faktur;
    }

    public String getKode_konsumen() {
        return kode_konsumen;
    }

    public void setKode_konsumen(String kode_konsumen) {
        this.kode_konsumen = kode_konsumen;
    }

    public String getKode_barang() {
        return kode_barang;
    }

    public void setKode_barang(String kode_barang) {
        this.kode_barang = kode_barang;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getHarga_satuan() {
        return harga_satuan;
    }

    public void setHarga_satuan(String harga_satuan) {
        this.harga_satuan = harga_satuan;
    }

    public String getHarga_total() {
        return harga_total;
    }

    public void setHarga_total(String harga_total) {
        this.harga_total = harga_total;
    }

    public koneksi getDb() {
        return db;
    }

    public void setDb(koneksi db) {
        this.db = db;
    }
    
    
    
    public void simpan() {
        
        String sql="INSERT INTO tbl_penjualan values('"+no_faktur+"','"+tgl_faktur+"',"
                + "'"+kode_konsumen+"','"+kode_barang+"','"+jumlah+"','"+harga_satuan+"','"+harga_total+"')";
        db.simpanData(sql);
        
    }
    
    public void hapus(){
        String sql="DELETE FROM tbl_penjualan WHERE no_faktur='"+no_faktur+"'";
        db.simpanData(sql);
        System.out.println(sql);
    }
    
    public List tampil() {
        List<PenjualanModel> data = new ArrayList<PenjualanModel>();
        ResultSet rs = null;
 
        try {
            String sql = "select * from tbl_penjualan order by no_faktur asc";
            rs = db.ambilData(sql);
            while (rs.next()) {
                PenjualanModel pm = new PenjualanModel();
                pm.setNo_faktur(rs.getString("no_faktur"));
                pm.setTgl_faktur(rs.getString("tgl_faktur"));
                pm.setKode_konsumen(rs.getString("kode_konsumen"));
                pm.setKode_barang(rs.getString("kode_barang"));
                pm.setJumlah(rs.getString("jumlah"));
                pm.setHarga_satuan(rs.getString("harga_satuan"));
                pm.setHarga_total(rs.getString("harga_total"));
                data.add(pm);
            }
            db.diskoneksi(rs);
        } catch (Exception ex) {
                System.out.println("Terjadi Kesalahan Saat menampilkan data Barang" + ex);
        }
        return data;
    }
    
    public List cariID() {
        List<PenjualanModel> data = new ArrayList<PenjualanModel>();
        ResultSet rs = null;
 
        try {
            String sql = "SELECT * FROM tbl_penjualan WHERE no_faktur='"+no_faktur+"'";
            rs = db.ambilData(sql);
            while (rs.next()) {
                PenjualanModel pm = new PenjualanModel();
                pm.setNo_faktur(rs.getString("no_faktur"));
                pm.setTgl_faktur(rs.getString("tgl_faktur"));
                pm.setKode_konsumen(rs.getString("kode_konsumen"));
                pm.setKode_barang(rs.getString("kode_barang"));
                pm.setJumlah(rs.getString("jumlah"));
                pm.setHarga_satuan(rs.getString("harga_satuan"));
                pm.setHarga_total(rs.getString("harga_total"));
                data.add(pm);
 
            }
            db.diskoneksi(rs);
        } catch (Exception ex) {
            System.out.println("Terjadi Kesalah Saat menampilkan Cari ID" + ex);
        }
        return data;
    }
                                                    
    
}

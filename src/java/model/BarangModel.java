
package model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import koneksi.koneksi;


public class BarangModel {
    String kode_barang, nama_barang, harga_jual,
           harga_beli, kode_satuan, kode_kategori, stok_barang;

    koneksi db = null;
    
    public BarangModel(){
        db = new koneksi();
    }

    public String getKode_barang() {
        return kode_barang;
    }

    public void setKode_barang(String kode_barang) {
        this.kode_barang = kode_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getHarga_jual() {
        return harga_jual;
    }

    public void setHarga_jual(String harga_jual) {
        this.harga_jual = harga_jual;
    }

    public String getHarga_beli() {
        return harga_beli;
    }

    public void setHarga_beli(String harga_beli) {
        this.harga_beli = harga_beli;
    }

    public String getKode_satuan() {
        return kode_satuan;
    }

    public void setKode_satuan(String kode_satuan) {
        this.kode_satuan = kode_satuan;
    }

    public String getKode_kategori() {
        return kode_kategori;
    }

    public void setKode_kategori(String kode_kategori) {
        this.kode_kategori = kode_kategori;
    }

    public String getStok_barang() {
        return stok_barang;
    }

    public void setStok_barang(String stok_barang) {
        this.stok_barang = stok_barang;
    }

    
    public koneksi getDb() {
        return db;
    }

    public void setDb(koneksi db) {
        this.db = db;
    }
    
    public void simpan(){
        
        String sql="INSERT INTO tbl_barang values('"+kode_barang+"','"+nama_barang+"',"
                + "'"+harga_jual+"','"+harga_beli+"','"+kode_satuan+"','"+kode_kategori+"','"+stok_barang+"')";
        db.simpanData(sql);
        
//        String sql="INSERT INTO tbl_barang(kode_barang,nama_barang,harga_jual,harga_beli,kode_satuan,kode_kategori) values('?','?','?','?','?','?')";
//        db.simpanData(sql);
        
//        String NAMA_TABEL = "tbl_barang";
//        String sql=" INSERT INTO " + NAMA_TABEL + " (kode_barang, nama_barang, harga_jual, "
//                + "harga_beli, kode_satuan, kode_kategori)" + " VALUES (?, ?, ?, ?, ?, ?)";
//        db.simpanData(sql);
    }
    
    public void update(){
        String sql="UPDATE tbl_barang SET nama_barang='"+nama_barang+"',harga_jual='"+harga_jual+"',"
                + "harga_beli='"+harga_beli+"',kode_satuan='"+kode_satuan+"',kode_kategori='"+kode_kategori+"',stok_barang='"+stok_barang+"'"
                + "WHERE kode_barang='"+kode_barang+"'";
        db.simpanData(sql);
        System.out.println(sql);
    }
    
     public void hapus(){
        String sql="DELETE FROM tbl_barang WHERE kode_barang='"+kode_barang+"'";
        db.simpanData(sql);
        System.out.println(sql);
    }
    
    public List tampil() {
        List<BarangModel> data = new ArrayList<BarangModel>();
        ResultSet rs = null;
 
        try {
            String sql = "select * from tbl_barang order by kode_barang asc";
            rs = db.ambilData(sql);
            while (rs.next()) {
                BarangModel bm = new BarangModel();
                bm.setKode_barang(rs.getString("kode_barang"));
                bm.setNama_barang(rs.getString("nama_barang"));
                bm.setHarga_jual(rs.getString("harga_jual"));
                bm.setHarga_beli(rs.getString("harga_beli"));
                bm.setKode_satuan(rs.getString("kode_satuan"));
                bm.setKode_kategori(rs.getString("kode_kategori"));
                bm.setStok_barang(rs.getString("stok_barang"));
                data.add(bm);
 
            }
            db.diskoneksi(rs);
        } catch (Exception ex) {
                System.out.println("Terjadi Kesalahan Saat menampilkan data Barang" + ex);
        }
        return data;
    }
    
    public List cariID() {
        List<BarangModel> data = new ArrayList<BarangModel>();
        ResultSet rs = null;
 
        try {
            String sql = "SELECT * FROM tbl_barang WHERE kode_barang='"+kode_barang+"'";
            rs = db.ambilData(sql);
            while (rs.next()) {
                BarangModel bm = new BarangModel();
                bm.setKode_barang(rs.getString("kode_barang"));
                bm.setNama_barang(rs.getString("nama_barang"));
                bm.setHarga_jual(rs.getString("harga_jual"));
                bm.setHarga_beli(rs.getString("harga_beli"));
                bm.setKode_satuan(rs.getString("kode_satuan"));
                bm.setKode_kategori(rs.getString("kode_kategori"));
                bm.setStok_barang(rs.getString("stok_barang"));
                data.add(bm);
 
            }
            db.diskoneksi(rs);
        } catch (Exception ex) {
            System.out.println("Terjadi Kesalah Saat menampilkan Cari ID" + ex);
        }
        return data;
 
    }
    
    public List cekStok(){
        List<BarangModel> data = new ArrayList<BarangModel>();
        ResultSet rs = null;
        
        try {
            String sql = "SELECT stok_barang FROM tbl_barang WHERE kode_barang='"+kode_barang+"'";
            rs = db.ambilData(sql);
            while (rs.next()) {
                BarangModel pm = new BarangModel();
                pm.setStok_barang(rs.getString("stok_barang"));
                data.add(pm);
            }
            db.diskoneksi(rs);
        } catch (Exception ex) {
            System.out.println("Terjadi Kesalah Saat menampilkan Cari ID" + ex);
        }
        return data;
   }
    
    

    
}

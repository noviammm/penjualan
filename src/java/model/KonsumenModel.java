
package model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import koneksi.koneksi;

public class KonsumenModel {
    String kode_konsumen, nama_konsumen, alamat, telp, password, email;

    koneksi db = null;
    
    public KonsumenModel(){
        db = new koneksi();
    }

    public String getKode_konsumen() {
        return kode_konsumen;
    }

    public void setKode_konsumen(String kode_konsumen) {
        this.kode_konsumen = kode_konsumen;
    }

    public String getNama_konsumen() {
        return nama_konsumen;
    }

    public void setNama_konsumen(String nama_konsumen) {
        this.nama_konsumen = nama_konsumen;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public koneksi getDb() {
        return db;
    }

    public void setDb(koneksi db) {
        this.db = db;
    }
    
    public void simpan(){
        
        String sql="INSERT INTO tbl_konsumen values('"+kode_konsumen+"','"+nama_konsumen+"',"
                + "'"+alamat+"','"+telp+"','"+password+"','"+email+"')";
        db.simpanData(sql);
    }
    
    public void update(){
        String sql="UPDATE tbl_konsumen SET nama_konsumen='"+nama_konsumen+"',alamat='"+alamat+"',"
                + "telp='"+telp+"',password='"+password+"',email='"+email+"'"
                + "WHERE kode_konsumen='"+kode_konsumen+"'";
        db.simpanData(sql);
        System.out.println(sql);
    }
    
     public void hapus(){
        String sql="DELETE FROM tbl_konsumen WHERE kode_konsumen='"+kode_konsumen+"'";
        db.simpanData(sql);
        System.out.println(sql);
    }
    
    public List tampil() {
        List<KonsumenModel> data = new ArrayList<KonsumenModel>();
        ResultSet rs = null;
 
        try {
            String sql = "select * from tbl_konsumen order by kode_konsumen asc";
            rs = db.ambilData(sql);
            while (rs.next()) {
                KonsumenModel km = new KonsumenModel();
                km.setKode_konsumen(rs.getString("kode_konsumen"));
                km.setNama_konsumen(rs.getString("nama_konsumen"));
                km.setAlamat(rs.getString("alamat"));
                km.setTelp(rs.getString("telp"));
//                km.setPassword(rs.getString("password"));
                km.setEmail(rs.getString("email"));
                data.add(km);
 
            }
            db.diskoneksi(rs);
        } catch (Exception ex) {
                System.out.println("Terjadi Kesalahan Saat menampilkan data Konsumen" + ex);
        }
        return data;
    }
    
    public List cariID() {
        List<KonsumenModel> data = new ArrayList<KonsumenModel>();
        ResultSet rs = null;
 
        try {
            String sql = "SELECT * FROM tbl_konsumen WHERE kode_konsumen='"+kode_konsumen+"'";
            rs = db.ambilData(sql);
            while (rs.next()) {
                KonsumenModel km = new KonsumenModel();
                km.setKode_konsumen(rs.getString("kode_konsumen"));
                km.setNama_konsumen(rs.getString("nama_konsumen"));
                km.setAlamat(rs.getString("alamat"));
                km.setTelp(rs.getString("telp"));
                km.setPassword(rs.getString("password"));
                km.setEmail(rs.getString("email"));
                data.add(km);
 
            }
            db.diskoneksi(rs);
        } catch (Exception ex) {
            System.out.println("Terjadi Kesalah Saat menampilkan Cari ID" + ex);
        }
        return data;
 
    }
    
    
}

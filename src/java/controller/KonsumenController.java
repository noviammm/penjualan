
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.KonsumenModel;

@WebServlet(name = "KonsumenController", urlPatterns = {"/KonsumenController"})
public class KonsumenController extends HttpServlet {
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                String proses=request.getParameter("proses");
                String action=request.getParameter("action");
                if (proses.equals("show-konsumen")){
                    response.sendRedirect("KonsumenView.jsp");
                return;
                }else if (proses.equals("input-konsumen")){
                    response.sendRedirect("tambahKonsumen.jsp");
                return;
                }else if(proses.equals("edit-konsumen")){
                    response.sendRedirect("editKonsumen.jsp?id="+request.getParameter("id"));
                return;
                }else if(proses.equals("hapus-konsumen")){
                    KonsumenModel km=new KonsumenModel();
                    km.setKode_konsumen(request.getParameter("id"));
                    km.hapus();
                    response.sendRedirect("KonsumenView.jsp");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                String data = request.getParameter("data");
                String proses = request.getParameter("proses");

            if (data != null){
                if(data.equals("konsumen")){
                    KonsumenModel km = new KonsumenModel();
                    km.setKode_konsumen(request.getParameter("kode_konsumen"));
                    km.setNama_konsumen(request.getParameter("nama_konsumen"));
                    km.setAlamat(request.getParameter("alamat"));
                    km.setTelp(request.getParameter("telp"));
                    km.setPassword(request.getParameter("password"));
                    km.setEmail(request.getParameter("email"));
                        if (proses.equals("show-konsumen")){
                            response.sendRedirect("KonsumenView.jsp");
                        return;
                        }else if (proses.equals("input-konsumen")){
                            km.simpan();
                        }else if (proses.equals("update-konsumen")){
                            km.update();
                        }else if(proses.equals("hapus-konsumen")){
                            km.hapus();
                        }
                    response.sendRedirect("KonsumenView.jsp");
                    
                }
            }
    }

}


package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.BarangModel;


@WebServlet(name = "BarangController", urlPatterns = {"/BarangController"})
public class BarangController extends HttpServlet {
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                String proses=request.getParameter("proses");
                String action=request.getParameter("action");
                if (proses.equals("show-barang")){
                    response.sendRedirect("BarangView.jsp");
                return;
                }else if (proses.equals("input-barang")){
                    response.sendRedirect("tambahBarang.jsp");
                return;
                }else if(proses.equals("edit-barang")){
                    response.sendRedirect("editBarang.jsp?id="+request.getParameter("id"));
                return;
                }else if(proses.equals("hapus-barang")){
                    BarangModel hm=new BarangModel();
                    hm.setKode_barang(request.getParameter("id"));
                    hm.hapus();
                    response.sendRedirect("BarangView.jsp");
        }
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                String data = request.getParameter("data");
                String proses = request.getParameter("proses");

            if (data != null){
                if(data.equals("barang")){
                    BarangModel bm = new BarangModel();
                    bm.setKode_barang(request.getParameter("kode_barang"));
                    bm.setNama_barang(request.getParameter("nama_barang"));
                    bm.setHarga_jual(request.getParameter("harga_jual"));
                    bm.setHarga_beli(request.getParameter("harga_beli"));
                    bm.setKode_satuan(request.getParameter("kode_satuan"));
                    bm.setKode_kategori(request.getParameter("kode_kategori"));
                    bm.setStok_barang(request.getParameter("stok_barang"));
                        if (proses.equals("show-barang")){
                            response.sendRedirect("BarangView.jsp");
                        return;
                        }else if (proses.equals("input-barang")){
                            bm.simpan();
                        }else if (proses.equals("update-barang")){
                            bm.update();
                        }else if(proses.equals("hapus-barang")){
                            bm.hapus();
                        }
                    response.sendRedirect("BarangView.jsp");
                    
                }
            }
    }
}



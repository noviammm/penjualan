
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.PenjualanModel;
import model.BarangModel;

/**
 *
 * @author Novia
 */
@WebServlet(name = "PenjualanController", urlPatterns = {"/PenjualanController"})
public class PenjualanController extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                String proses=request.getParameter("proses");
                String action=request.getParameter("action");
                if (proses.equals("show-penjualan")){
                    response.sendRedirect("PenjualanView.jsp");
                return;
                }else if (proses.equals("input-penjualan")){
                    response.sendRedirect("inputPenjualan.jsp");
                return;
                }else if(proses.equals("hapus-penjualan")){
                    PenjualanModel hm=new PenjualanModel();
                    hm.setNo_faktur(request.getParameter("id"));
                    hm.hapus();
                    response.sendRedirect("PenjualanView.jsp");
        }
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                String data = request.getParameter("data");
                String proses = request.getParameter("proses");
             
            if (data != null){
                if(data.equals("penjualan")){
                    PenjualanModel pm = new PenjualanModel();
                    pm.setNo_faktur(request.getParameter("no_faktur"));
                    pm.setTgl_faktur(request.getParameter("tgl_faktur"));
                    pm.setKode_konsumen(request.getParameter("kode_konsumen"));
                    pm.setKode_barang(request.getParameter("kode_barang"));
                    pm.setJumlah(request.getParameter("jumlah"));
                    pm.setHarga_satuan(request.getParameter("harga_satuan"));
                    pm.setHarga_total(request.getParameter("harga_total"));
                        
                    if (proses.equals("show-penjualan")){
                        response.sendRedirect("PenjualanView.jsp");
                    return;
                    }else if (proses.equals("input-penjualan")){
                        pm.simpan();
                    }else if(proses.equals("hapus-penjualan")){
                        pm.hapus();
                    }
                    
                    response.sendRedirect("PenjualanView.jsp");
                    
                }
            }
    }
}
